/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import Logging from "../utils/Logging";
const log = Logging.log();

import {
	IConnection,
	InitializeResult,
	TextDocuments,
    TextDocument,
} from "vscode-languageserver";

import { IAssemblerResults, ILine } from "../assembler/KickAssembler";
import SettingsProvider from "../providers/SettingsProvider";
import CompletionProvider from "../providers/CompletionProvider";
import DiagnosticProvider from "../providers/DiagnosticProvider";
import HoverProvider from "../providers/HoverProvider";
import { ISettings } from "../providers/SettingsProvider";
import Project, { ISymbol } from "./Project";
import SignatureHelpProvider from "../providers/SignatureHelpProvider";

export default class ProjectManager {

    private _connection:IConnection;
    private _documents:TextDocuments;
    private _workspaceRoot:string;
    private _currentProject?:Project;

    private _settingsProvider:SettingsProvider;
    private _completionProvider:CompletionProvider;
    private _diagnosticProvider:DiagnosticProvider;
    private _hoverProvider:HoverProvider;
    private _signatureHelpProvider:SignatureHelpProvider;

    private _processIncludes;
    private _changeCount;

    private _timer:NodeJS.Timer;

    constructor(connection:IConnection) {

        //console.log(connection);
        this._connection = connection;
        this._currentProject = new Project();

        //  init hooks
        this._documents = new TextDocuments();
        this._documents.listen(this._connection);

        //  init
        this._connection.onInitialize((params):InitializeResult => {
            this._workspaceRoot = params.rootUri || "";

            return {
                //  tell client about server capabilities
                capabilities: {
                    //  working in FULL text document synch mode
                    textDocumentSync: this._documents.syncKind,
                    
                    //  hover on symbols
                    hoverProvider: true,

                    //  code complete
                    completionProvider: {
                        resolveProvider: true,
                        triggerCharacters: ["#", ".", " "],
                    },
                    
                    //  signature help
                    signatureHelpProvider: {
                        "triggerCharacters": [ '(' ],
                    }
                },
            };
        });

        //  create providers
        const projectInfoProvider = {
            //getResults: this.getAssemblerResults.bind(this),
            getSource: this.getCurrentSource.bind(this),
            getSettings: this.getSettings.bind(this),
            getLines: this.getLines.bind(this),
            getAssemblerResults: this.getNewAssemblerResults.bind(this),
            getSymbols: this.getSymbols.bind(this)
        };

        this._completionProvider = new CompletionProvider(this._connection, projectInfoProvider);
        this._settingsProvider = new SettingsProvider(this._connection, projectInfoProvider);
        this._diagnosticProvider = new DiagnosticProvider(this._connection, projectInfoProvider);
        this._hoverProvider = new HoverProvider(this._connection, projectInfoProvider);
        this._signatureHelpProvider = new SignatureHelpProvider(this._connection, projectInfoProvider);

        /**
         * Handles Processing when a File is Opened
         * 
         * resets the includes processing flag so that
         * we do a sweep of includes when the process
         * fpr changed content is triggered
         */
        this._documents.onDidOpen((change) => { 
            log.trace("[ProjectManager] onDidOpen");
            this._currentProject.assemble(this.getSettings(), change.document,true);
            this._diagnosticProvider.process(change.document.uri);
            //this._connection.sendNotification("ERROR", "From Language Server");
});

        /**
         * Handles Processing when a File is Changed
         * 
         * assembles and processes the file, and if
         * needed will process any include files
         * 
         * we assume that when a person is working on 
         * a document that the include files are not
         * being changed at the same time
         */
        this._documents.onDidChangeContent((change) => {
            log.trace("[ProjectManager] onDidChangeContent");
            log.trace(`includesProcessed: ${this._processIncludes}`);
            this._currentProject.assemble(this.getSettings(), change.document, false);
            //this._currentProject.updateSource(this.getSettings(), change.document);
            if (this._timer) {
                clearTimeout(this._timer);
            }
            this._timer = setTimeout(() => {
                log.trace("timeout activated");
                this._currentProject.assemble(this.getSettings(), change.document, false);
                this._diagnosticProvider.process(change.document.uri);
                this._processIncludes = false;
            },960);
        });

        /**
         * Handles Processing when a File is Saved
         * 
         * assembles and processes the currently open
         * file with include files
         */
        this._documents.onDidSave((change) => {
            log.trace("[ProjectManager] onDidSave");
            if (this._timer) {
                clearTimeout(this._timer);
            }
            this._currentProject.assemble(this.getSettings(), change.document, true);
            this._diagnosticProvider.process(change.document.uri);
        });

    }

    public start() {
        this._connection.listen();
        //console.log("listening...");
    }

    private getNewAssemblerResults():IAssemblerResults|undefined {
        if (this._currentProject) {
            return this._currentProject.getNewAssemblerResults();
        }
    }

    //private getAssemblerResults():IAssemblerResult|undefined {
    //    if (this._currentProject) {
    //        return this._currentProject.getAssemblerResults();
    //    }
    //}

    private getCurrentSource():string[]|undefined {
        if(this._currentProject) {
            return this._currentProject.getSourceLines();
        }
    }

    private getSettings():ISettings {
        return this._settingsProvider.getCurrent();
    }

    private getLines(textDocument:TextDocument):ILine[]|undefined {
        return this._currentProject.getLines(textDocument);
    }

    private getSymbols():ISymbol[]|undefined {
        return this._currentProject.getSymbols();
    }

}
