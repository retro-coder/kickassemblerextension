/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import Logging from "../utils/Logging";
const log = Logging.log();

import StringUtils from "../utils/StringUtils";

import { 
    IAssemblerInfo,
    IDirective,
    IError,
    IFile, 
    ILibrary,
    ILine,
    IPreprocessorDirective,
    ISourceRange, 
    ISyntax } from "../assembler/KickAssembler";

enum ReadModes {
    None,
    Errors, 
    Directives,
    Libraries,
    Preprocessor,
    Syntax,
    Files,
}

export class AssemblerInfo implements IAssemblerInfo {

    libraries: ILibrary[];
    directives: IDirective[];
    preprocessorDirectives: IPreprocessorDirective[];
    files: IFile[];
    syntax: ISyntax[];
    errors: IError[];

    constructor(data:string) {
        var lines:string[] = data.toString().split("\n");
        this.libraries = [];
        this.directives = [];
        this.preprocessorDirectives = [];
        this.files = [];
        this.syntax = [];
        this.errors = [];
        this.parse(lines);
    }

    private parse(lines:string[]) {
        log.trace('[AssemblerInfo] parse');

        let mode:ReadModes = ReadModes.None;

        for(var i = 0; i < lines.length; i++) {

            var process = true;
            var line = lines[i];

            //  remove unwanted stuffs
            line = StringUtils.removeCRLF(line);

            if (line.length <= 0) {
                continue;
            }

            if (line.toLowerCase() == '[libraries]') {
                process = false;
                mode = ReadModes.Libraries;
            }

            if (line.toLowerCase() == '[errors]') {
                process = false;
                mode = ReadModes.Errors;
            }

            if (line.toLowerCase() == '[directives]') {
                process = false;
                mode = ReadModes.Directives;
            }

            if (line.toLowerCase() == '[syntax]') {
                process = false;
                mode = ReadModes.Syntax;
            }

            if (line.toLowerCase() == '[files]') {
                process = false;
                mode = ReadModes.Files;
            }

            if (line.toLowerCase() == '[ppdirectives]') {
                process = false;
                mode = ReadModes.Preprocessor;
            }

            if (process) {

                if (mode == ReadModes.Errors) {
                    this.errors.push(this.parseError(line));
                }

                if (mode == ReadModes.Libraries) {
                    this.libraries.push(this.parseLibrary(line));
                }
                
                if (mode == ReadModes.Directives) {
                    this.directives.push(this.parseDirective(line));
                }
                
                if (mode == ReadModes.Preprocessor) {
                    this.preprocessorDirectives.push(this.parsePreprocessorDirective(line));
                }
                
                if (mode == ReadModes.Syntax) {
                    this.syntax.push(this.parseSyntax(line));
                }
                
                if (mode == ReadModes.Files) {
                    this.files.push(this.parseFile(line));
                }
                
            }
            
        }
    }

    private parseDirective(line:string):IDirective {
        log.fine('[AssemblerInfo] parseDirective');
        let parms = line.split(";");
        let directive = <IDirective>{};
        directive.name = parms[0];
        directive.example = parms[1];
        directive.description = parms[2];
        return directive;
    }

    private parseFile(line:string):IFile {
        log.fine('[AssemblerInfo] parseFile');
        let parms = line.split(';');
        let file = <IFile> {};
        file.index = parseInt(parms[0]);
        file.uri = parms[1];
        return file;
    }

    /**
     * Parse an Error from a Line of Text
     * @param line 
     */
    private parseError(line:string):IError {
        log.fine('[AssemblerInfo] parseError');
        let parms = line.split(';');
        let error = <IError>{};
        error.level = parms[0];
        error.range = this.parseRange(parms[1]);
        error.message = parms[2];
        return error;
    }

    private parseLibrary(line:string):ILibrary {
        log.fine('[AssemblerInfo] parseLibrary');
        let parms = line.split(";");
        let library = <ILibrary>{};
        library.name = parms[0];
        library.entry = parms[1];
        library.data = parms[2];
        return library;
    }

    private parsePreprocessorDirective(line:string):IPreprocessorDirective {
        log.fine('[AssemblerInfo] parsePreprocessorDirective');
        let parms = line.split(";");
        let ppdirective = <IPreprocessorDirective>{};
        ppdirective.name = parms[0];
        ppdirective.example = parms[1];
        ppdirective.description = parms[2];
        return ppdirective;
    }

    private parseSyntax(line:string):ISyntax {
        log.fine('[AssemblerInfo] parseSyntax');
        let parms = line.split(";");
        let syntax = <ISyntax>{};
        syntax.type = parms[0];
        syntax.sourceRange = this.parseRange(parms[1]);
        syntax.line = syntax.sourceRange.startLine;
        return syntax;
    }

    private parseRange(text:string):ISourceRange {
        log.fine('[AssemblerInfo] parseRange');
        let parms = text.split(",");
        let range = <ISourceRange>{};
        range.startLine = Number(parms[0]) - 1;
        range.startPosition = Number(parms[1]) - 1;
        range.endLine = Number(parms[2]) - 1;
        range.endPosition = Number(parms[3]);
        range.fileIndex = Number(parms[4]);
        return range;
    }

}