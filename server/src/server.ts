/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/

import {
	createConnection,
	IConnection,
	IPCMessageReader,
	IPCMessageWriter,
} from "vscode-languageserver";

import ProjectManager from "./project/ProjectManager";

// Create a connection for the server. The connection uses Node's IPC as a transport
const connection:IConnection = createConnection(new IPCMessageReader(process), new IPCMessageWriter(process));
const projectManager = new ProjectManager(connection);
projectManager.start();
