/*
	Copyright (C) Paul Hocker. All rights reserved.
	Licensed under the MIT License. See License.txt in the project root for license information.
*/


'use strict';

import * as path from 'path';
import * as vscode from 'vscode';

import { workspace, Disposable, ExtensionContext, commands, window } from 'vscode';
import { LanguageClient, LanguageClientOptions, SettingMonitor, ServerOptions, TransportKind } from 'vscode-languageclient';
import { CommandRun } from './commands/CommandRun';
import { CommandBuild } from './commands/CommandBuild';
import { CommandDebug } from './commands/CommandDebug';
import ConfigUtils from './utils/ConfigUtils';


export function activate(context: ExtensionContext) {

	var _outputChannel: vscode.OutputChannel;
	_outputChannel = window.createOutputChannel('Kick Assembler Build');

	//	the server is implemented in node
	let serverModule = context.asAbsolutePath(path.join('server', 'server.js'));
	// 	the debug options for the server
	let debugOptions = { execArgv: ["--nolazy", "--inspect=6009"] };
	console.log(debugOptions);

	// If the extension is launched in debug mode then the debug server options are used
	// Otherwise the run options are used
	let serverOptions: ServerOptions = {
		run: { module: serverModule, transport: TransportKind.ipc },
		debug: { module: serverModule, transport: TransportKind.ipc, options: debugOptions }
	}

	// Options to control the language client
	let clientOptions: LanguageClientOptions = {
		// Register the server for plain text documents
		documentSelector: ['kickassembler'],
		synchronize: {
			// Synchronize the setting section 'languageServerExample' to the server
			configurationSection: 'kickassembler',
			// Notify the server about file changes to '.clientrc files contain in the workspace
			fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
		}
	}

	context.subscriptions.push(vscode.workspace.onDidChangeConfiguration(configChanged));

	// Create the language client and start the client.
	let languageServer = new LanguageClient('kickassembler', 'Kick Assembler Language Extension Client', serverOptions, clientOptions);
	let disposable = languageServer.start();

	// Push the disposable to the context's subscriptions so that the 
	// client can be deactivated on extension deactivation
	context.subscriptions.push(disposable);

	//	wait for the language server to be ready
	languageServer.onReady().then(() => {

		//	catch notifications from the language server
		languageServer.onNotification("ERROR", (message: string) => {
			vscode.window.showErrorMessage(message);
		});

	});

	//	create command for assembling
	let cmdBuild = commands.registerCommand("kickassembler.build", function () {
		commandBuild(context, _outputChannel);
	});

	//	create command to run
	let cmdRun = commands.registerCommand("kickassembler.run", function () {
		commandRun(context, _outputChannel);
	});

	//	create command to build & run
	let cmdBuildRun = commands.registerCommand("kickassembler.buildandrun", function () {
		commandBuildRun(context, _outputChannel);
	});

	let cmdBuildDebug = commands.registerCommand("kickassembler.buildanddebug", function () {
		commandBuildDebug(context, _outputChannel);
	});

	context.subscriptions.push(cmdBuild);
	context.subscriptions.push(cmdRun);
	context.subscriptions.push(cmdBuildRun);
	context.subscriptions.push(cmdBuildDebug);

	ConfigUtils.validateSettings();

	console.log("kickass-vscode-ext extension is active.");
}

export function deactivate() {
	console.log("kickass-vscode-ext extension has been deactivated.");
}

function commandBuild(context: ExtensionContext, output: vscode.OutputChannel): number {
	console.log("[ClientExtension] commandCompile");
	if (!ConfigUtils.validateSettings()) {
		vscode.window.showErrorMessage("Cannot Build - Check Settings");
		return;
	}
	var cb = new CommandBuild(context, output);
	return cb.build(output);
}

function commandRun(context: ExtensionContext, output: vscode.OutputChannel) {
	console.log("[ClientExtension] commandRun");
	if (!ConfigUtils.validateSettings()) {
		vscode.window.showErrorMessage("Cannot Run - Check Settings");
		return;
	}
	var cr = new CommandRun(context, output);
	cr.run(output)
}

function commandDebug(context: ExtensionContext, output: vscode.OutputChannel) {
	console.log("[ClientExtension] commandDebug");
	if (!ConfigUtils.validateSettings()) {
		vscode.window.showErrorMessage("Cannot Debug - Check Settings");
		return;
	}
	var cr = new CommandDebug(context, output);
	cr.run(output)
}

function commandBuildRun(context: ExtensionContext, output: vscode.OutputChannel) {
	console.log("[ClientExtension] commandCompileRun");
	if (!ConfigUtils.validateSettings()) {
		vscode.window.showErrorMessage("Cannot Build and Run - Check Settings");
		return;
	}
	window.activeTextEditor.document.save().then(function (reponse) {
		if (commandBuild(context, output) == 0) {
			commandRun(context, output);
		}
	});
}

function commandBuildDebug(context: ExtensionContext, output: vscode.OutputChannel) {
	console.log("[ClientExtension] commandCompileRun");
	if (!ConfigUtils.validateSettings()) {
		vscode.window.showErrorMessage("Cannot Build and Run - Check Settings");
		return;
	}
	window.activeTextEditor.document.save().then(function (reponse) {
		if (commandBuild(context, output) == 0) {
			commandDebug(context, output);
		}
	});
}

function configChanged(params) {
	ConfigUtils.validateSettings();
}