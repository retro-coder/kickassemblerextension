# Changelog

All notable changes to the KickAssemblerExtension for VSCode are listed here.

## 0.9.6 
### Changed
- changed name of project to KickAssemblerExtension for VSCode
- updated tmLanguage to 1.15 from Sublime project
- renamed all settings and references to 'kickassembler'
### Fixes
- fixed some undefined errors in eval, const and var directives

## 0.9.5.1
### Added
### Fixes
- [#8](https://gitlab.com/Retro65/kickass-vscode-ext/issues/8) - output from compiler should be reused
- [#7](https://gitlab.com/Retro65/kickass-vscode-ext/issues/7) - completion item errors
- [#6](https://gitlab.com/Retro65/kickass-vscode-ext/issues/6) - provide example configs 
- [#5](https://gitlab.com/Retro65/kickass-vscode-ext/issues/5) - compile failing without feedback
- [#4](https://gitlab.com/Retro65/kickass-vscode-ext/issues/4) - show converted numbers
- [#3](https://gitlab.com/Retro65/kickass-vscode-ext/issues/3) - fix build pipeline

## 0.9.5
### Added
- #import file support
- scope support (experimental)
- source verification delay (experimental)
- macros, functions and pseudocommands from #import files
- invalid settings feedback for remediation
### Changed
- definitions sourced from assembler
- processor directives sourced from assembler
- upgrade to newest language server protocol 
- symbol support completely redone to make other providers easier to implement
- added hover information for numeric/hex/binary/decimal values
- update to gitlab build script to accomodate new project structure
### Fixes
- [#2](https://gitlab.com/Retro65/kickass-vscode-ext/issues/2) - incorrect comment block marker
- [#1](https://gitlab.com/Retro65/kickass-vscode-ext/issues/1) - errors on completion provider
### Issues
- signature help *might* occur on macros and functions
- hover thinks EVERYTHING is a symbol

## 0.9.4
### Added
- emulator runtime options
- compile program (assemble)
- compile & run (F5)
- run emulator on current project
- document save on compile & run
### Issues
- only supports running PRG files currently

## 0.9.2
### Added
- limited symbol scoping (breaks some things too)
### Changed
- support for posix (OSX & Linux) and Win32 paths
### Minor
- changed GitLab building when Tagged only

## 0.9.0 (2017-07-01)
### Added
- syntax highlighting
- inline error detection
- basic symbol/label hover support
- autocomplete for instructions, symbols and labels
### Minor
- autobuild on GitLab


## Legend
- **Added**     - new additions to the extension
- **Changed**   - modification to existing features
- **Removed**   - feature removed
- **Minor**     - minor update
- **Fixes**     - fixes in this release
- **Issues**    - known issues in this release

